<?php

namespace Parfeon\Helper;

/**
 * Factory for creating Helpers
 * Class HelperFactory
 * @package Parfeon\Helper
 */
class HelperFactory
{
    /**
     * @var self
     */
    protected static $helperFactory;

    /**
     * @var HelperInterface[]
     */
    protected $storage;

    /**
     * @var string[]
     */
    protected $namespaces = [
       'Yolva\Helpers'
    ];

    /**
     * @return HelperFactory
     */
    public static function getInstance()
    {
        if(self::$helperFactory === null){
            self::$helperFactory = new self();
        }

        return self::$helperFactory;
    }

    /**
     * @param $name
     * @return mixed
     * @throws HelperNotFoundException
     */
    public function get($name)
    {
        $className = $this->getClassName($name);

        if(!($fullClassName = $this->findNamespace($className))){
            throw new HelperNotFoundException('Helper class '.$className.' is not found');
        }

        if(isset($this->storage[$fullClassName])){
            return $this->storage[$fullClassName];
        }

        $helper = new $fullClassName();

        $this->storage[$fullClassName] = $helper;

        return $this->storage[$fullClassName];
    }

    /**
     * @param $namespace
     */
    public function addNamespace($namespace)
    {
        if(!in_array($namespace, $this->namespaces)){
            $this->namespaces[] = $namespace;
        }
    }

    /**
     * @param $name
     * @return string
     */
    protected function getClassName($name)
    {
        if(strpos($name,'-') !== false){
            $name = str_replace('-', '', ucwords($name, '-'));
        }

        if(strpos($name, '_') !== false){
            $name = str_replace('_', '', ucwords($name, '_'));
        }

        $name = ucfirst($name);

        if(strpos('Helper',$name) === false){
            $name .='Helper';
        }

        return $name;
    }

    /**
     * @param $className
     * @return false|string
     */
    protected function findNamespace($className)
    {
        if(class_exists($className)){
            return $className;
        }

        foreach ($this->namespaces as $namespace){
            if(class_exists($namespace.'\\'.$className)) {
                return $namespace.'\\'.$className;
            }
        }

        return false;
    }
}