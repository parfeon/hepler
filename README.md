## A simple factory for creating and storing Helpers

```php
use Parfeon\HelperFactory;

// Add namespace to array of namespaces
HelperFactory::getInstance()->addNamespace('Project\Helpers');

// Get helper (Project\Helpers\MoneyHelper)
$helper = HelperFactory::getInstance()->get('money');

// Get helper (Project\Helpers\BudgetLineHelper)
$helper = HelperFactory::getInstance()->get('budget-line');

// Get helper (Project\Helpers\CalculatePriceHelper)
$helper = HelperFactory::getInstance()->get('calculate_price');

```